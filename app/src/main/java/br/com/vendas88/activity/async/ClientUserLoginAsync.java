package br.com.vendas88.activity.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import br.com.vendas88.R;
import br.com.vendas88.activity.MainActivity;
import br.com.vendas88.endpoint.ClientUserEndPoint;
import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ClientUserResponseVo;
import br.com.vendas88.vo.ClientUserVo;
import br.com.vendas88.vo.Message;
import br.com.vendas88.vo.TypeMessageEnun;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ClientUserLoginAsync extends AsyncTask<ClientUserVo, Void, ClientUserResponseVo> {


    private Context context = null;
    private ProgressDialog pDialog = null;
    private Retrofit retrofit;
    private ClientUserEndPoint clientUserEndPoint;

    public ClientUserLoginAsync(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.lbl_loading_data));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        super.onPreExecute();
    }

    @Override
    protected ClientUserResponseVo doInBackground(ClientUserVo... params) {

        ClientUserResponseVo clientUserResponseVo = new ClientUserResponseVo();
        clientUserResponseVo.setMessage(new Message("Erro ao tentar fazer o login", TypeMessageEnun.ERROR));

        ClientUserVo clientUserVo = params[0];

        try {
            retrofit = new Retrofit.Builder().
                    baseUrl(Vendas88Util.URL_SERVER).
                    client(Vendas88Util.getOkHttpClient()).
                    addConverterFactory(GsonConverterFactory.create()).build();

            clientUserEndPoint = retrofit.create(ClientUserEndPoint.class);
            Call<ClientUserResponseVo> call = clientUserEndPoint.login(clientUserVo);
            Response<ClientUserResponseVo> response = call.execute();

            if (response.isSuccess() && response.body() != null) {
                clientUserResponseVo = response.body();
            }

        } catch (Exception e) {
            Log.e("Falha ao fazer login",e.getMessage());
        }

        return clientUserResponseVo;
    }

    @Override
    protected void onPostExecute(ClientUserResponseVo clientUserResponseVo) {

        pDialog.dismiss();
        String msg = clientUserResponseVo.getMessage().getMessage();
        Toast  toast = Toast.makeText(this.context, msg, Toast.LENGTH_LONG);

        if (clientUserResponseVo.getMessage().getType().equals(TypeMessageEnun.SUCESS)) {
            Vendas88Util.setClientUserVo(clientUserResponseVo.getVo());
            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);
        } else {
            toast.show();
        }

        super.onPostExecute(clientUserResponseVo);
    }
}
