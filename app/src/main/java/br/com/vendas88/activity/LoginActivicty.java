package br.com.vendas88.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.vendas88.R;
import br.com.vendas88.activity.async.ClientUserLoginAsync;
import br.com.vendas88.vo.ClientUserVo;


public class LoginActivicty extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    public static class PlaceholderFragment extends Fragment {

        private Button buttonCreateAccountLogin;
        private Button buttonLoginLogin;
        private EditText editTextEmailLogin;
        private EditText editTextPassLogin;
        private TextView textViewTitleActionBar;

        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

            final View rootView = inflater.inflate(R.layout.fragment_login, container, false);

            buttonLoginLogin = (Button) rootView.findViewById(R.id.buttonLoginLogin);
            buttonCreateAccountLogin = (Button) rootView.findViewById(R.id.buttonCreateAccountLogin);
            editTextPassLogin = (EditText) rootView.findViewById(R.id.editTextPassLogin);
            editTextEmailLogin = (EditText) rootView.findViewById(R.id.editTextEmailLogin);

            textViewTitleActionBar= (TextView) rootView.findViewById(R.id.textViewTitleActionBar);
            textViewTitleActionBar.setText(R.string.lbl_enter_you_account);

            buttonLoginLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ClientUserVo vo = new ClientUserVo();
                    vo.setEmail(editTextEmailLogin.getText().toString());
                    vo.setPass(editTextPassLogin.getText().toString());
                    ClientUserLoginAsync clientUserLoginAsync = new ClientUserLoginAsync(getContext());
                    clientUserLoginAsync.execute(vo);
                }
            });

            buttonCreateAccountLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), CreateClientActivity.class);
                    startActivity(intent);
                }
            });

            return rootView;
        }


    }


}
