package br.com.vendas88.activity.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import br.com.vendas88.activity.listener.TaskListProductListener;
import br.com.vendas88.endpoint.ProductEndPoint;
import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.FiltroProduct;
import br.com.vendas88.vo.ReturnProductVos;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ProductListAsync extends AsyncTask<FiltroProduct, Void, ReturnProductVos>{

    private Context context = null;
    private Retrofit retrofit;
    private ProductEndPoint productEndPoint;
    private TaskListProductListener taskListProductListener;

    public ProductListAsync(Context context ,TaskListProductListener taskListProductListener) {
        this.context = context;
        this.taskListProductListener = taskListProductListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected ReturnProductVos doInBackground(FiltroProduct... params) {

        ReturnProductVos retorno = null;

        try {

            FiltroProduct filtroProduct = params[0];
            retrofit = new Retrofit.Builder().
                    baseUrl(Vendas88Util.URL_APIMOBILE).
                    client(Vendas88Util.getOkHttpClient()).
                    addConverterFactory(GsonConverterFactory.create()).build();

            productEndPoint = retrofit.create(ProductEndPoint.class);
            Call<ReturnProductVos> call =null;
            Response<ReturnProductVos> response = call.execute();

            if (response.isSuccess() && response.body()!=null) {
                retorno = response.body();
            }

        } catch (Exception e) {
            Log.e(getClass().getName(),"Falha ao cadastrar produto",e);
        }

        return retorno;

    }


    @Override
    protected void onPostExecute(ReturnProductVos productReturnVo) {

        if (productReturnVo!=null && productReturnVo.getProductVos()!=null) {
            taskListProductListener.onCompleteTaskProductListSucess(productReturnVo);
        }else{
            taskListProductListener.onCompleteTaskProductListError(productReturnVo);
        }

        super.onPostExecute(productReturnVo);
    }




}
