package br.com.vendas88.activity.listener;

import br.com.vendas88.vo.ReturnProductVos;

/**
 * Created by adansbento on 05/05/16.
 */
public interface TaskListProductListener {
    void onCompleteTaskProductListSucess(ReturnProductVos returnProductVo);
    void onCompleteTaskProductListError(ReturnProductVos returnProductVo);
}