package br.com.vendas88.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.vendas88.R;
import br.com.vendas88.util.Vendas88Util;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private TextView textViewEmailNavigation;
    private TextView textViewNameUserNavigation;
    private ImageView imageViewNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);

        textViewEmailNavigation = (TextView) findViewById(R.id.textViewEmailNavigation);
        textViewNameUserNavigation =(TextView) findViewById(R.id.textViewNameUserNavigation);
        imageViewNavigation = (ImageView) findViewById(R.id.imageViewNavigation);


        if(Vendas88Util.getClientUserVo()!=null &&
                Vendas88Util.getClientUserVo().getEmail()!=null&&
                Vendas88Util.getClientUserVo().getName()!=null){

            int endText = Vendas88Util.getClientUserVo().getLogin().indexOf(" ");
            if(endText==-1){endText=Vendas88Util.getClientUserVo().getName().length();}
            textViewNameUserNavigation.setText(Vendas88Util.getClientUserVo().getName().toCharArray(),0,endText);
            textViewNameUserNavigation.setVisibility(View.VISIBLE);
            textViewEmailNavigation.setText(Vendas88Util.getClientUserVo().getEmail());
            textViewEmailNavigation.setVisibility(View.VISIBLE);
            imageViewNavigation.setVisibility(View.VISIBLE);
        }else{
            textViewEmailNavigation.setText("");
            textViewEmailNavigation.setVisibility(View.INVISIBLE);
            textViewNameUserNavigation.setText("");
            textViewNameUserNavigation.setVisibility(View.INVISIBLE);
            imageViewNavigation.setVisibility(View.INVISIBLE);
        }
        // display the first navigation drawer view on app launch
        displayView(0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.action_search) {
            Toast.makeText(getApplicationContext(), "Search action is selected!", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        Intent intent = null;

        switch (position) {
            case 0:
                fragment = new SalesFragment();
                title = getString(R.string.title_sales);
                break;
            case 1:
                fragment = new ProductFragment();
                title = getString(R.string.title_product);
                break;
            case 2:
                fragment = new PaymentFormFragment();
                title = getString(R.string.title_payment_form);
                break;
            case 3:
                fragment = new DiscountRulesFragment();
                title = getString(R.string.title_discount_rules);
                break;
            case 4:
                fragment = new ReportFragment();
                title = getString(R.string.title_discount_rules);
                break;
            default:
                break;
        }


        if(intent!=null){
           startActivity(intent);
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }
}