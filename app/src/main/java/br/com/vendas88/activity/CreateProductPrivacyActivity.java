package br.com.vendas88.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;

import br.com.vendas88.R;
import br.com.vendas88.vo.ProductVo;

public class CreateProductPrivacyActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_create_product_privacy);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    public static class PlaceholderFragment extends Fragment {

        private TextView textViewTitleActionBar;
        private ProductVo productVo;
        private CheckBox checkBoxViewAllProductPrivacy;
        private CheckBox checkBoxOnlyFriendsProductPrivacy;
        private CheckBox checkBoxNearProductPrivacy;
        private SeekBar seekBarNearProductPrivacy;
        private CheckBox checkBoxEmailProductPrivacy;
        private CheckBox checkBoxPhoneProductPrivacy;
        private TextView textViewValueDistance;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


            final View rootView = inflater.inflate(R.layout.fragment_product_privacy, container, false);
            textViewTitleActionBar = (TextView) rootView.findViewById(R.id.textViewTitleActionBar);
            checkBoxViewAllProductPrivacy =  (CheckBox) rootView.findViewById(R.id.checkBoxViewAllProductPrivacy);
            checkBoxOnlyFriendsProductPrivacy =  (CheckBox) rootView.findViewById(R.id.checkBoxOnlyFriendsProductPrivacy);
            checkBoxNearProductPrivacy =  (CheckBox) rootView.findViewById(R.id.checkBoxNearProductPrivacy);
            seekBarNearProductPrivacy =  (SeekBar) rootView.findViewById(R.id.seekBarNearProductPrivacy);
            checkBoxEmailProductPrivacy =  (CheckBox) rootView.findViewById(R.id.checkBoxEmailProductPrivacy);
            checkBoxPhoneProductPrivacy=  (CheckBox) rootView.findViewById(R.id.checkBoxPhoneProductPrivacy);
            textViewValueDistance = (TextView) rootView.findViewById(R.id.textViewValueDistance);

            textViewTitleActionBar.setText(R.string.lbl_privacy_advert);

            productVo = (ProductVo) savedInstanceState.getSerializable("product");

            checkBoxEmailProductPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            checkBoxPhoneProductPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            checkBoxViewAllProductPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(checkBoxViewAllProductPrivacy.isChecked()){
                        checkBoxViewAllProductPrivacy.setEnabled(false);
                        checkBoxOnlyFriendsProductPrivacy.setEnabled(false);
                    }
                    checkBoxViewAllProductPrivacy.setEnabled(true);
                    checkBoxOnlyFriendsProductPrivacy.setEnabled(true);

                }
            });


            checkBoxOnlyFriendsProductPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });


            checkBoxNearProductPrivacy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(checkBoxNearProductPrivacy.isChecked()) {
                        seekBarNearProductPrivacy.setEnabled(true);
                    }else{
                        seekBarNearProductPrivacy.setEnabled(false);
                    }
                }
            });


            seekBarNearProductPrivacy.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    textViewValueDistance.setText(progress);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });

            return rootView;
        }
    }
}
