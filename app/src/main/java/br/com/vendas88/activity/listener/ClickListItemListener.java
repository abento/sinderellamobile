package br.com.vendas88.activity.listener;

import android.view.View;

/**
 * Created by adansbento on 05/05/16.
 */
public interface ClickListItemListener {

    void onClickListItem(View v, int position);
}
