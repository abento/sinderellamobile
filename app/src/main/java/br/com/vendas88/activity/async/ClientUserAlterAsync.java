package br.com.vendas88.activity.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import br.com.vendas88.R;
import br.com.vendas88.activity.MainActivity;
import br.com.vendas88.endpoint.ClientUserEndPoint;
import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ClientUserResponseVo;
import br.com.vendas88.vo.ClientUserVo;
import br.com.vendas88.vo.Message;
import br.com.vendas88.vo.TypeMessageEnun;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ClientUserAlterAsync extends AsyncTask<ClientUserVo, Void, ClientUserResponseVo> {


    private Context context = null;
    private ProgressDialog pDialog = null;
    private Retrofit retrofit;
    private ClientUserEndPoint clientUserEndPoint;

    public ClientUserAlterAsync(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.lbl_loading_data));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        super.onPreExecute();
    }

    @Override
    protected ClientUserResponseVo doInBackground(ClientUserVo... params) {

        ClientUserResponseVo clientUserResponseVo = new ClientUserResponseVo();
        clientUserResponseVo.setMessage(new Message("", TypeMessageEnun.ERROR));

        ClientUserVo clientUserVo = params[0];

        try {
            retrofit = new Retrofit.Builder().
                    baseUrl(Vendas88Util.URL_SERVER).
                    client(Vendas88Util.getOkHttpClient()).
                    addConverterFactory(GsonConverterFactory.create()).build();

            clientUserEndPoint = retrofit.create(ClientUserEndPoint.class);
            Call<ClientUserResponseVo> call = clientUserEndPoint.update(clientUserVo);
            Response<ClientUserResponseVo> response = call.execute();

            if (response.isSuccess() && response.body() != null) {
                clientUserResponseVo = response.body();
            }

        } catch (Exception e) {
            clientUserResponseVo.setMessage(new Message("Erro ao tentar criar usuário tente novamente", TypeMessageEnun.ERROR));
        }

        return clientUserResponseVo;
    }

    @Override
    protected void onPostExecute(ClientUserResponseVo clientUserResponseVo) {

        pDialog.dismiss();
        Toast toast = null;
        String msg = clientUserResponseVo.getMessage().getMessage();

        if (clientUserResponseVo.getMessage().getType().equals(TypeMessageEnun.SUCESS)) {
            toast = Toast.makeText(this.context, R.string.usuario_criado_com_sucesso, Toast.LENGTH_LONG);
            Vendas88Util.setClientUserVo(clientUserResponseVo.getVo());
            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);
        } else {
            toast = Toast.makeText(this.context, msg, Toast.LENGTH_LONG);
        }

        toast.show();
        super.onPostExecute(clientUserResponseVo);
    }
}
