package br.com.vendas88.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.vendas88.R;
import br.com.vendas88.activity.async.ClientUserAlterAsync;
import br.com.vendas88.util.MaskDate;
import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ClientUserVo;

public class ProfileActivicty extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    public static class PlaceholderFragment extends Fragment {

        private Button buttonEditProfile;
        private EditText editTextFulltNameProfile;
        private EditText editTextEmailProfile;
        private EditText editTextCodePostalProfile;
        private EditText editTextAddressProfile;
        private EditText editTextAddressComplementProfile;
        private EditText editTextLoginPassword;
        private EditText editTextCellphoneProfile;
        private EditText editTextBirthdateProfile;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

            buttonEditProfile = (Button) rootView.findViewById(R.id.buttonCreateProfile);

            editTextFulltNameProfile = (EditText) rootView.findViewById(R.id.editTextFulltNameProfile);

            editTextEmailProfile = (EditText) rootView.findViewById(R.id.editTextEmailProfile);
            editTextEmailProfile.setText(Vendas88Util.getClientUserVo().getEmail());

            editTextCodePostalProfile = (EditText) rootView.findViewById(R.id.editTextCodePostalProfile);

            editTextAddressProfile = (EditText) rootView.findViewById(R.id.editTextAddressProfile);

            editTextAddressComplementProfile = (EditText) rootView.findViewById(R.id.editTextAddressComplementProfile);

            editTextCellphoneProfile = (EditText) rootView.findViewById(R.id.editTextCellPhoneProfile);

            editTextBirthdateProfile = (EditText) rootView.findViewById(R.id.editTextBirthDateProfile);

            editTextBirthdateProfile.addTextChangedListener(new MaskDate(editTextBirthdateProfile));

            TextView textViewTitleActionBar = (TextView) rootView.findViewById(R.id.textViewTitleActionBar);
            textViewTitleActionBar.setText(R.string.lbl_edit_profile);

            buttonEditProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buttonCreateProfileOnListener();
                }
            });

            return rootView;
        }


        void buttonCreateProfileOnListener() {

            ClientUserVo vo = new ClientUserVo();
            vo.setId(Vendas88Util.getClientUserVo().getId());
            vo.setEmail(editTextEmailProfile.getText().toString());
            //vo.setPass(editTextLoginPassword.getText().toString());
            ClientUserAlterAsync clientUserAlterAsync = new ClientUserAlterAsync(getContext());
            clientUserAlterAsync.execute(vo);
        }

        private boolean validateFields() {
            boolean result = true;
            return result;
        }


    }
}
