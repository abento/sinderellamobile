package br.com.vendas88.activity.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import br.com.vendas88.R;
import br.com.vendas88.activity.LoginActivicty;
import br.com.vendas88.endpoint.ClientEndPoint;
import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ClientResponseVo;
import br.com.vendas88.vo.ClientVo;
import br.com.vendas88.vo.TypeMessageEnun;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ClientCreateAsync extends AsyncTask<ClientVo, Void, ClientResponseVo> {

    private Context context = null;
    private ProgressDialog pDialog = null;
    private Retrofit retrofit;
    private ClientEndPoint clientEndPoint;

    public ClientCreateAsync(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.lbl_loading_data));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        super.onPreExecute();
    }

    @Override
    protected ClientResponseVo doInBackground(ClientVo... params) {

        ClientResponseVo clientResponseVo = new ClientResponseVo();

        try {
            ClientVo clientVo = params[0];
            retrofit = new Retrofit.Builder().
                    baseUrl(Vendas88Util.URL_SERVER).
                    client(Vendas88Util.getOkHttpClient()).
                    addConverterFactory(GsonConverterFactory.create()).build();

            clientEndPoint = retrofit.create(ClientEndPoint.class);
            Call<ClientResponseVo> call = clientEndPoint.create(clientVo);
            Response<ClientResponseVo> response = call.execute();
            clientResponseVo=response.body();
        } catch (Exception e) {
            clientResponseVo=null;
        }

        return clientResponseVo;
    }

    @Override
    protected void onPostExecute(ClientResponseVo clientUserResponseVo) {

        pDialog.dismiss();
        Toast toast = null;
        String msg;

        if (clientUserResponseVo == null || clientUserResponseVo.getMessage() == null) {
            msg = "Erro ao tentar criar usuário tente novamente";
        } else {

            msg = clientUserResponseVo.getMessage().getMessage();
            if (clientUserResponseVo.getMessage().getType().equals(TypeMessageEnun.SUCESS)) {
                toast = Toast.makeText(this.context, R.string.usuario_criado_com_sucesso, Toast.LENGTH_LONG);
                Intent intent = new Intent(context, LoginActivicty.class);
                context.startActivity(intent);
            }

        }
        toast = Toast.makeText(this.context, msg, Toast.LENGTH_LONG);
        toast.show();
        super.onPostExecute(clientUserResponseVo);
    }
}
