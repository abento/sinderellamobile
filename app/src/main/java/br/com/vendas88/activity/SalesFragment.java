package br.com.vendas88.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import br.com.vendas88.R;
import br.com.vendas88.activity.async.ProductListAsync;
import br.com.vendas88.activity.listener.ClickListItemListener;
import br.com.vendas88.activity.listener.TaskListProductListener;
import br.com.vendas88.model.adapter.ProductListAdapter;
import br.com.vendas88.vo.FiltroProduct;
import br.com.vendas88.vo.ReturnProductVos;

public class SalesFragment  extends Fragment implements TaskListProductListener, ClickListItemListener {

    private final int MAX_RESULT = 100;
    private RecyclerView recyclerViewListProduct;
    private FloatingActionButton floatBtnAddProduct;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public SalesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_sales, container, false);

        recyclerViewListProduct = (RecyclerView) rootView.findViewById(R.id.recyclerViewListProductHome);
        recyclerViewListProduct.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewListProduct.setLayoutManager(mLayoutManager);


        floatBtnAddProduct = (FloatingActionButton)rootView.findViewById(R.id.floatBtnAddProduct);
        floatBtnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(rootView.getContext(), CreateProductActivicty.class);
                startActivity(intent);
            }
        });

        ProductListAsync productListAsync = new ProductListAsync(getContext(),this);
        FiltroProduct filtroProduct = new FiltroProduct();
        filtroProduct.setMaxResult(MAX_RESULT);
        productListAsync.execute(filtroProduct);

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onCompleteTaskProductListSucess(ReturnProductVos productReturnVo) {
        mAdapter = new ProductListAdapter(productReturnVo.getProductVos());
        recyclerViewListProduct.setAdapter(mAdapter);
    }

    @Override
    public void onCompleteTaskProductListError(ReturnProductVos productReturnVo) {
        Toast.makeText(getContext(), R.string.fail_in_try_access_server,1000);
    }

    @Override
    public void onClickListItem(View v, int position) {

    }
}