package br.com.vendas88.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import br.com.vendas88.R;

public class Splash extends Activity implements Runnable {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Handler handler = new Handler();
        handler.postDelayed(this, 2000);
    }


    public void run(){
        startActivity(new Intent(this, LoginActivicty.class));
        finish();
    }
}