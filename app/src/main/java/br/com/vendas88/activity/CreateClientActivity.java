package br.com.vendas88.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.vendas88.R;
import br.com.vendas88.activity.async.ClientCreateAsync;
import br.com.vendas88.vo.ClientUserVo;
import br.com.vendas88.vo.ClientVo;

public class CreateClientActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_create_client);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    public static class PlaceholderFragment extends Fragment {

        private Button saveCreateClientBT;

        private EditText enterpriseNameCreateClientET;
        private EditText emailCreateClientET;
        private EditText documentCreateClientET;
        private EditText clientNameCreateClientET;
        private EditText passwordCreateClientET;
        private TextView textViewTitleActionBar;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_create_client, container, false);

            saveCreateClientBT = (Button) rootView.findViewById(R.id.buttonSaveCreateClient);
            enterpriseNameCreateClientET = (EditText) rootView.findViewById(R.id.editTextEnterpriseNameCreateClient);
            documentCreateClientET = (EditText) rootView.findViewById(R.id.editTextDocumentCreateClient);
            clientNameCreateClientET = (EditText) rootView.findViewById(R.id.editTextClientNameCreateClient);
            emailCreateClientET = (EditText) rootView.findViewById(R.id.editTextEmailAdmCreateClient);
            passwordCreateClientET = (EditText) rootView.findViewById(R.id.editTextPasswordCreateClient);

            textViewTitleActionBar = (TextView) rootView.findViewById(R.id.textViewTitleActionBar);
            textViewTitleActionBar.setText(R.string.lbl_new_client);
            saveCreateClientBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   saveClientOnListener();
                }
            });

            return rootView;
        }


        void saveClientOnListener() {

            ClientUserVo clientUserVo = new ClientUserVo();
            clientUserVo.setName(clientNameCreateClientET.getText().toString());
            clientUserVo.setDocument(documentCreateClientET.getText().toString());
            clientUserVo.setEmail(emailCreateClientET.getText().toString());
            clientUserVo.setPass(passwordCreateClientET.getText().toString());

            ClientVo clientVo = new ClientVo();
            clientVo.setClientUser(clientUserVo);
            clientVo.setDocument(documentCreateClientET.getText().toString());
            clientVo.setNameClient(enterpriseNameCreateClientET.getText().toString());

            ClientCreateAsync clientUserSigninAsync = new ClientCreateAsync(getContext());
            clientUserSigninAsync.execute(clientVo);

        }

    }
}
