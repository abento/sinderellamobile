package br.com.vendas88.activity.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import br.com.vendas88.R;
import br.com.vendas88.activity.MainActivity;
import br.com.vendas88.endpoint.ProductEndPoint;
import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ReturnProductVo;
import br.com.vendas88.vo.ProductVo;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ProductCreateAsync extends AsyncTask<ProductVo, Void, ReturnProductVo> {

    private Context context = null;
    private ProgressDialog pDialog = null;
    private Retrofit retrofit;
    private ProductEndPoint productEndPoint;


    public ProductCreateAsync(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.lbl_loading_data));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        super.onPreExecute();
    }

    @Override
    protected ReturnProductVo doInBackground(ProductVo... params) {

        ReturnProductVo retorno = null;

        try {

            ProductVo productVo = params[0];
            retrofit = new Retrofit.Builder().
                    baseUrl(Vendas88Util.URL_APIMOBILE).
                    client(Vendas88Util.getOkHttpClient()).
                    addConverterFactory(GsonConverterFactory.create()).build();

            productEndPoint = retrofit.create(ProductEndPoint.class);
            Call<ReturnProductVo> call = productEndPoint.createProduct(productVo);
            Response<ReturnProductVo> response = call.execute();

            if (response.isSuccess() && response.body()==null) {
                retorno = response.body();
            }

        } catch (Exception e) {
            Log.e(getClass().getName(),"Falha ao cadastrar produto",e);
        }

        return retorno;

    }


    @Override
    protected void onPostExecute(ReturnProductVo returnProductVo) {

        pDialog.dismiss();
        Toast toast = null;

        if (returnProductVo !=null && returnProductVo.getVos()!=null) {
            toast = Toast.makeText(this.context, R.string.usuario_criado_com_sucesso, Toast.LENGTH_LONG);
            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);
        } else {
            toast = Toast.makeText(this.context, "Erro ao criar anúncio!", Toast.LENGTH_LONG);
        }

        toast.show();
        super.onPostExecute(returnProductVo);
    }


}
