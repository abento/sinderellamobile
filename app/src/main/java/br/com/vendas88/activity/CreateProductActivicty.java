package br.com.vendas88.activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.vendas88.R;
import br.com.vendas88.activity.async.ProductCreateAsync;
import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ClientUserVo;
import br.com.vendas88.vo.ProductVo;

public class CreateProductActivicty extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_create_product);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    public static  class PlaceholderFragment extends Fragment {

        private static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 1;
        private Button btnProductNew;
        private TextView textViewTitleActionBar;
        private EditText editTextTitle;
        private EditText editTextDescription;
        private EditText editTextPrice;
        private Spinner spinnerSize;
        private Spinner spinnerType;
        private Uri outputFileUri;
        private ImageView imageViewPhotoCreatProduct;
        private ImageView imageViewPrivacyCreatProduct;
        private RadioButton radioButtonUsed;
        private ProductVo productVo = new ProductVo();

        @Override
        public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

            final View rootView = inflater.inflate(R.layout.fragment_create_product, container, false);
            btnProductNew = (Button) rootView.findViewById(R.id.buttonCreateProductNewProduct);

            textViewTitleActionBar= (TextView) rootView.findViewById(R.id.textViewTitleActionBar);
            textViewTitleActionBar.setText(R.string.lbl_new_product);

            editTextTitle = (EditText) rootView.findViewById(R.id.editTextTitleCreateProduct);
            editTextDescription = (EditText) rootView.findViewById(R.id.editTextDescriptionCreateProduct);
            editTextPrice =(EditText) rootView.findViewById(R.id.editTextPriceCreateProduct);

            radioButtonUsed = (RadioButton) rootView.findViewById(R.id.radioButtonUsedCreateProduct);
            imageViewPhotoCreatProduct = (ImageView) rootView.findViewById(R.id.imageViewPhotoCreatProduct);

            imageViewPrivacyCreatProduct = (ImageView) rootView.findViewById(R.id.imageViewPrivacyCreatProduct);
            imageViewPrivacyCreatProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(rootView.getContext(), CreateProductPrivacyActivity.class);
                    intent.putExtra("product", (Parcelable) productVo);
                    rootView.getContext().startActivity(intent);
                }
            });

            imageViewPhotoCreatProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openImageIntent();
                }
            });

            btnProductNew.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProductCreateAsync productCreateAsync = new ProductCreateAsync(getContext());
                    productCreateAsync.execute(fillProduct());
                }


            });

            return rootView;
        }
        private ProductVo fillProduct() {
            productVo.setPrice((BigDecimal) editTextPrice.getText());
            fillClientUser(productVo);
            return  productVo;
        }

        private void fillClientUser(ProductVo productVo) {

            ClientUserVo clientUserVo = Vendas88Util.getClientUserVo();

            if(clientUserVo!=null){
            }
        }


        private void openImageIntent() {

        // Determine Uri of camera image to save.
            final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
            root.mkdirs();
            final String fname = String.valueOf(System.currentTimeMillis());
            final File sdImageMainDirectory = new File(root, fname);
            outputFileUri = Uri.fromFile(sdImageMainDirectory);

            // Camera.
            final List<Intent> cameraIntents = new ArrayList<Intent>();
            final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            final PackageManager packageManager = getContext().getPackageManager();
            final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
            for(ResolveInfo res : listCam) {
                final String packageName = res.activityInfo.packageName;
                final Intent intent = new Intent(captureIntent);
                intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
                intent.setPackage(packageName);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                cameraIntents.add(intent);
            }

            // Filesystem.
            final Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

            // Chooser of filesystem options.
            final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

            // Add the camera options.
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

            startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (resultCode == RESULT_OK) {
                if (requestCode == YOUR_SELECT_PICTURE_REQUEST_CODE) {
                    final boolean isCamera;
                    if (data == null) {
                        isCamera = true;
                    } else {
                        final String action = data.getAction();
                        if (action == null) {
                            isCamera = false;
                        } else {
                            isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        }
                    }

                    Uri selectedImageUri;
                    if (isCamera) {
                        selectedImageUri = outputFileUri;
                    } else {
                        selectedImageUri = data == null ? null : data.getData();
                    }
                }
            }
        }

    }


}
