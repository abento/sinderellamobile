package br.com.vendas88.model.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.vendas88.R;
import br.com.vendas88.activity.listener.ClickListItemListener;
import br.com.vendas88.model.viewholder.ProductListViewHolder;
import br.com.vendas88.vo.ProductVo;

public class ProductListAdapter extends  RecyclerView.Adapter<ProductListViewHolder> {

    private List<ProductVo> productVos;
    private ClickListItemListener clickListItemListener;

    public ProductListAdapter(List<ProductVo> productVos){
        this.productVos = productVos;
    }

    @Override
    public ProductListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product_list, parent, false);
        ProductListViewHolder vh = new ProductListViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(ProductListViewHolder holder, int position) {
        holder.getTextViewPrice().setText(productVos.get(position).getPrice().toString());
    }

    @Override
    public int getItemCount() {
        return productVos.size();
    }

    public void setClickListItemListener(ClickListItemListener clickListItemListener) {
        this.clickListItemListener = clickListItemListener;
    }
}

