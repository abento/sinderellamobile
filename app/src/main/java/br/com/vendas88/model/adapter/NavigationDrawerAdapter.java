package br.com.vendas88.model.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import br.com.vendas88.R;
import br.com.vendas88.model.NavDrawerItem;

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {

    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        chooseImage(holder,current);
    }

    private void chooseImage(MyViewHolder holder,NavDrawerItem current) {

        final String title = current.getTitle();

        switch (title){

            case "Amigas":
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_favorite_black_18dp, 0, 0, 0);
                 break;
            case "Sapateira":
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_redeem_black_18dp, 0, 0, 0);
                break;
            case "Notificações":
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_mail_outline_black_18dp, 0, 0, 0);
                break;
            case "Desconectar":
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_sentiment_dissatisfied_black_18dp, 0, 0, 0);
                break;
            case "Conectar":
                holder.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_sentiment_satisfied_black_18dp, 0, 0, 0);
                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }
}