package br.com.vendas88.model.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.com.vendas88.R;

public class ProductListViewHolder extends RecyclerView.ViewHolder {

  private TextView textViewTitle;
  private TextView textViewPrice;
  private TextView textViewSize;


    public ProductListViewHolder(View itemView) {
        super(itemView);
        textViewTitle = (TextView) itemView.findViewById(R.id.textViewTitleProductList);
        textViewPrice = (TextView) itemView.findViewById(R.id.textViewPriceProductList);
        textViewSize = (TextView) itemView.findViewById(R.id.textViewSizeProductList);
    }

    public TextView getTextViewTitle() {
        return textViewTitle;
    }

    public TextView getTextViewPrice() {
        return textViewPrice;
    }

    public TextView getTextViewSize() {
        return textViewSize;
    }
}
