package br.com.vendas88.vo;

public class Message {
	
	private String message;
	private TypeMessageEnun type;
	
	
	public Message(String message, TypeMessageEnun type) {
		this.message = message;
		this.type = type;
	}

	public Message(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public TypeMessageEnun getType() {
		return type;
	}
	public void setType(TypeMessageEnun type) {
		this.type = type;
	}

}
