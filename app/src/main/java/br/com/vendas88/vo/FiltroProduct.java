package br.com.vendas88.vo;


import java.util.Date;

public class FiltroProduct {
	

	private String id;
    private String title;
    private String description;
    private ClientUserVo clientUserVo;
    private String price;
    private Boolean isUsed;
    private Date dateCreated;
    private String raio;
    private Integer maxResult;
    
    public void setMaxResult(Integer maxResult) {
		this.maxResult = maxResult;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public ClientUserVo getClientUserVo() {
		return clientUserVo;
	}
	public void setClientUserVo(ClientUserVo clientUserVo) {
		this.clientUserVo = clientUserVo;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

	public Boolean getIsUsed() {
		return isUsed;
	}
	public void setIsUsed(Boolean isUsed) {
		this.isUsed = isUsed;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getRaio() {
		
		return raio;
	}
	public Integer getMaxResult() {
		return maxResult;
	}
    
	}