package br.com.vendas88.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SaleVo {

    private String id;
    private List<ItemSaleVo> itemSales = new ArrayList<>();
    private Date dateSale;
    private BigDecimal amountSale;
    private BigDecimal discountValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ItemSaleVo> getItemSales() {
        return itemSales;
    }

    public void setItemSales(List<ItemSaleVo> itemSales) {
        this.itemSales = itemSales;
    }

    public Date getDateSale() {
        return dateSale;
    }

    public void setDateSale(Date dateSale) {
        this.dateSale = dateSale;
    }

    public void setAmountSale(BigDecimal amountSale) {
        this.amountSale = amountSale;
    }

    public BigDecimal getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(BigDecimal discountValue) {
        this.discountValue = discountValue;
    }

    public BigDecimal getAccountValueWithDiscount() {

        if (getDiscountValue() !=null && !getAmountSale().equals(new BigDecimal("0"))){
            return getAmountSale().subtract(getDiscountValue());
        }
        return getAmountSale();
    }

    public BigDecimal getAmountSale() {

        if(itemSales != null  && !getItemSales().isEmpty()){
            amountSale = new BigDecimal(0);
            for (ItemSaleVo item:getItemSales() ) {
                amountSale = amountSale.add(item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getAmount())));
            }
        }
        return amountSale.setScale(2,BigDecimal.ROUND_CEILING);
    }
}
