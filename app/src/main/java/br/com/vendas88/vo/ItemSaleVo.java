package br.com.vendas88.vo;

/**
 * Created by adansbento on 13/12/16.
 */
public class ItemSaleVo {

    private String id;
    private int amount;
    private ProductVo product;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public ProductVo getProduct() {
        return product;
    }

    public void setProduct(ProductVo product) {
        this.product = product;
    }
}
