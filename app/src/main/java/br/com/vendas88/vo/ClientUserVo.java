package br.com.vendas88.vo;

import java.io.Serializable;
import java.util.Date;


public class ClientUserVo implements Serializable {

	private String id;
	private String name;
	private String login;
	private String pass;
	private String email;
	private String idClient;
	private String document;
	private Date dateLastLogin;
	private PerfilEnum Perfil;

	public ClientUserVo() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}


	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public Date getDateLastLogin() {
		return dateLastLogin;
	}

	public void setDateLastLogin(Date dateLastLogin) {
		this.dateLastLogin = dateLastLogin;
	}

	public PerfilEnum getPerfil() {
		return Perfil;
	}

	public void setPerfil(PerfilEnum perfil) {
		Perfil = perfil;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}