package br.com.vendas88.vo;

import java.util.List;

public class ReturnProductVos {

    private List<ProductVo> productVos;
    
    public List<ProductVo> getProductVos() {
        return productVos;
    }
}
