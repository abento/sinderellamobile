package br.com.vendas88.vo;

import java.util.Date;

public class ClientVo {

    private String id;
    private String nameClient;
    private String document;
    private String cellPhone;
    private ClientUserVo clientUser;
    private Date dateCreate;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public ClientUserVo getClientUser() {
        return clientUser;
    }

    public void setClientUser(ClientUserVo clientUser) {
        this.clientUser = clientUser;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }
}
