package br.com.vendas88.vo;

import java.util.List;

public class BaseVo<C> {

    private Message message;
    private C vo;
    private List<C> vos;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public C getVo() {
        return vo;
    }

    public void setVo(C vo) {
        this.vo = vo;
    }

    public List<C> getVos() {
        return vos;
    }

    public void setVos(List<C> vos) {
        this.vos = vos;
    }
}
