package br.com.vendas88.vo;

import br.com.vendas88.entity.ClientUser;

public class UsuarioVo {


    private Long id;
    private String nome;
    private String login;
    private String senha;
    private Long idCliente;
    private String perfil;

    public UsuarioVo(ClientUser usuario){
        setLogin(usuario.getLogin());
        setSenha(usuario.getSenha());
        setPerfil(usuario.getPerfil());
        setIdCliente(usuario.getIdCliente());
        setNome(usuario.getNome());
    }

    public UsuarioVo() {

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
}
