package br.com.vendas88.util;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import br.com.vendas88.vo.ClientUserVo;

public class Vendas88Util {

    // email vendas88app@gmail.com
    public static final String URL_SERVER = "http://192.168.0.11:8080";
    public static final String URL_APIMOBILE = "http://192.168.0.11:8080";
    public static final String ID_APP_GOOGLE = "502908855927";

    private static ClientUserVo clientUserVo;

    public static ClientUserVo getClientUserVo() {
        return clientUserVo;
    }

    public static boolean isLogin() {
        return clientUserVo == null ||
                clientUserVo.getEmail() == null ||
                clientUserVo.getId() == null;
    }

    public static void setClientUserVo(ClientUserVo clientUserVo) {
        Vendas88Util.clientUserVo = clientUserVo;
    }

    public static OkHttpClient getOkHttpClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(220 * 1000, TimeUnit.MILLISECONDS);
        okHttpClient.setReadTimeout(220 * 1000, TimeUnit.MILLISECONDS);

        okHttpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("_csrf", "c94c441b-7251-4cd0-9f7c-dbb8cb3f217a")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        return okHttpClient;
    }

}
