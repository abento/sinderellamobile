package br.com.vendas88.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public class MoneyUtil {



    public static BigDecimal stringToBigDecimal(final String value){

        BigDecimal bigDecimal = null;

        try {
            // Create a DecimalFormat that fits your requirements
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setGroupingSeparator(',');
            symbols.setDecimalSeparator('.');
            String pattern = "#,##0.0#";
            DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
            decimalFormat.setParseBigDecimal(true);
            // parse the string
            bigDecimal = (BigDecimal) decimalFormat.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  bigDecimal;
    }
}
