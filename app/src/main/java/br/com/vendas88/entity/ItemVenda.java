package br.com.vendas88.entity;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by adansbento on 21/11/16.
 */
public class ItemVenda {

    private Long id;
    @DatabaseField
    private Long idProduto;
    @DatabaseField
    private Long quantidade;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Long idProduto) {
        this.idProduto = idProduto;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }
}
