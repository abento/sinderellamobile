package br.com.vendas88.entity;

import com.j256.ormlite.field.DatabaseField;

import java.math.BigDecimal;

public class Produto {

    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField
    private Long idCliente;
    @DatabaseField
    private Long estoque;
    @DatabaseField
    private String nome;
    @DatabaseField
    private String loc;
    @DatabaseField
    private BigDecimal preco;
    @DatabaseField
    private BigDecimal precoCusto;
    @DatabaseField
    private boolean ativo = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public Long getEstoque() {
        return estoque;
    }

    public void setEstoque(Long estoque) {
        this.estoque = estoque;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public BigDecimal getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(BigDecimal precoCusto) {
        this.precoCusto = precoCusto;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
