package br.com.vendas88.entity;

import com.j256.ormlite.field.DatabaseField;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class Venda {

    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField
    private Date dataCompra;
    @DatabaseField
    private List<ItemVenda> itemVendas;
    @DatabaseField
    private BigDecimal valorDesconto;
    @DatabaseField
    private BigDecimal valorTotal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataCompra() {
        return dataCompra;
    }

    public void setDataCompra(Date dataCompra) {
        this.dataCompra = dataCompra;
    }

    public List<ItemVenda> getItemVendas() {
        return itemVendas;
    }

    public void setItemVendas(List<ItemVenda> itemVendas) {
        this.itemVendas = itemVendas;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }
}
