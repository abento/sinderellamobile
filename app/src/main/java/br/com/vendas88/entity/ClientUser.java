package br.com.vendas88.entity;

import com.j256.ormlite.field.DatabaseField;

import br.com.vendas88.vo.UsuarioVo;

public class ClientUser {


    @DatabaseField(generatedId = true,unique = true)
    private Long id;
    @DatabaseField
    private String nome;
    @DatabaseField(unique = true)
    private String login;
    @DatabaseField
    private String senha;
    @DatabaseField
    private Long idCliente;
    @DatabaseField
    private String perfil;

    public ClientUser(){}

    public ClientUser(UsuarioVo usuarioVo) {

        if(usuarioVo!=null) {
            setNome(usuarioVo.getNome());
            setId(usuarioVo.getId());
            setIdCliente(usuarioVo.getIdCliente());
            setPerfil(usuarioVo.getPerfil());
            setSenha(usuarioVo.getSenha());
            setLogin(usuarioVo.getLogin());
        }
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
}
