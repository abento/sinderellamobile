package br.com.vendas88.endpoint;

import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ClientResponseVo;
import br.com.vendas88.vo.ClientUserResponseVo;
import br.com.vendas88.vo.ClientVo;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface ClientEndPoint{

    String URL = Vendas88Util.URL_APIMOBILE+"/client";

    @POST(URL+"/new/")
    @Headers("Content-Type:application/json")
    Call<ClientResponseVo> create(@Body ClientVo clientUserVo);

    @POST(URL+"/update/")
    @Headers("Content-Type:application/json")
    Call<ClientResponseVo> update(@Body ClientVo clientUserVo);

    @POST(URL+"/logout/")
    @Headers("Content-Type:application/json")
    Call<ClientResponseVo> logout(@Body ClientVo clientUserVo);


}
