package br.com.vendas88.endpoint;

import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.ClientUserResponseVo;
import br.com.vendas88.vo.ClientUserVo;
import br.com.vendas88.vo.ClientVo;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface  ClientUserEndPoint {

    String URL = Vendas88Util.URL_APIMOBILE+"/clientuser";

    @POST(URL+"/new/")
    @Headers("Content-Type:application/json")
    Call<ClientUserResponseVo> create(@Body ClientUserVo clientUserVo);

    @POST(URL+"/all/")
    @Headers("Content-Type:application/json")
    Call<ClientUserResponseVo> findAll(@Body ClientUserVo clientUserVo);

    @POST(URL+"/update/")
    @Headers("Content-Type:application/json")
    Call<ClientUserResponseVo> update(@Body ClientUserVo clientUserVo);

    @POST(URL+"/login/")
    @Headers("Content-Type:application/json")
    Call<ClientUserResponseVo> login(@Body ClientUserVo clientUserVo);


}
