package br.com.vendas88.endpoint;

import br.com.vendas88.util.Vendas88Util;
import br.com.vendas88.vo.FiltroProduct;
import br.com.vendas88.vo.ProductVo;
import br.com.vendas88.vo.ReturnProductVo;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface ProductEndPoint {

    String URL = Vendas88Util.URL_APIMOBILE+"/clientuser";

    @POST(URL+"/new/")
    @Headers("Content-Type:application/json")
    Call<ReturnProductVo> createProduct(@Body ProductVo productVo);


    @POST(URL+"/all/")
    @Headers("Content-Type:application/json")
    Call<ReturnProductVos> findAll(@Body FiltroProduct filtroProduct);


    @POST(URL+"/update/")
    @Headers("Content-Type:application/json")
    Call<ReturnProductVos> update(@Body FiltroProduct filtroProduct);
}
