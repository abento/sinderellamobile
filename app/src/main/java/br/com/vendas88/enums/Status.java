package br.com.vendas88.enums;

public enum Status {

    INSERTED(1, "INSERIDO"),
    PUBLISHED(2, "PUBLICADO"),
    FINISHED(3, "FINALIZADO"),
    DELETED(4, "DELETADO");

    private int id;
    private String description;

    private Status(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }
}
