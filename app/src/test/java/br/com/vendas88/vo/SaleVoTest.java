package br.com.vendas88.vo;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

public class SaleVoTest {

    @Test
    public void testGetValorTotal() throws Exception {
        SaleVo saleVo = new SaleVo();
        saleVo.setId("11");
        saleVo.setDateSale(new Date());
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(10.33)));
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(100.33)));
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(90.33)));
        Assert.assertEquals(saleVo.getAmountSale(),new BigDecimal("200.99"));
    }

    @Test
    public void testGetValorTotalComDesconto(){
        SaleVo saleVo = new SaleVo();
        saleVo.setId("11");
        saleVo.setDateSale(new Date());
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(10.33)));
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(100.33)));
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(90.33)));
        saleVo.setDiscountValue(new BigDecimal(10));
        Assert.assertEquals(saleVo.getAccountValueWithDiscount(),new BigDecimal("190.99"));
    }

    @Test
    public void testGetValorTotalComDescontoSemValor(){
        SaleVo saleVo = new SaleVo();
        saleVo.setId("11");
        saleVo.setDateSale(new Date());
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(10.33)));
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(100.33)));
        saleVo.getItemSales().add(criarItemSaleVo(new BigDecimal(90.33)));
        Assert.assertTrue(saleVo.getAccountValueWithDiscount().equals(saleVo.getAmountSale()));
    }


    private ProductVo criarProduto(BigDecimal precoValor){
       ProductVo vo = new ProductVo();
       vo.setId("1");
       vo.setEnable(true);
       vo.setStock(10);
       vo.setDescription("AAABB + "+precoValor.toString());
       vo.setPrice(precoValor);
       vo.setPriceCost(precoValor.multiply(BigDecimal.valueOf(90/100)));
       return vo;
   }

    private ItemSaleVo criarItemSaleVo(BigDecimal precoValor){
        ItemSaleVo vo = new ItemSaleVo();
        vo.setProduct(criarProduto(precoValor));
        vo.setAmount(1);
        return vo;
    }

}